package benchmark;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

/*
Benchmark              Mode  Cnt       Score       Error  Units
StringConcat.buffer   thrpt   25  581961,329 ± 61902,286  ops/s
StringConcat.builder  thrpt   25  523557,253 ± 48957,952  ops/s
StringConcat.format   thrpt   25   12847,640 ±   794,420  ops/s
StringConcat.plus     thrpt   25  475382,972 ± 38061,372  ops/s
 */
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Threads(4)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Thread)
public class StringConcatBenchmark {

    private String num = String.valueOf(Integer.MAX_VALUE);

    @Benchmark
    public String plus() {
        return "Qwerty uiop asdfghjkl " + num + " zxcvbnm jhgfdsa";
    }

    @Benchmark
    public String format() {
        return String.format("Qwerty uiop asdfghjkl %s zxcvbnm jhgfdsa", num);
    }

    @Benchmark
    public String messageFormat() {
        return MessageFormat.format("Qwerty uiop asdfghjkl {0} zxcvbnm jhgfdsa", num);
    }

    @Benchmark
    public String builder() {
        return new StringBuilder("Qwerty uiop asdfghjkl").append(num).append(" zxcvbnm jhgfdsa").toString();
    }

    @Benchmark
    public String buffer() {
        return new StringBuffer("Qwerty uiop asdfghjkl").append(num).append(" zxcvbnm jhgfdsa").toString();
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(StringConcatBenchmark.class.getSimpleName())
//                .jvmArgs("-XX:+UnlockExperimentalVMOptions", "-XX:+UseEpsilonGC", "-Xmx24G", "-Xms24G", "-XX:+AlwaysPreTouch", "-Xlog:gc")
                .build();

        new Runner(opt).run();
    }
}
