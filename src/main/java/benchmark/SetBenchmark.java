package benchmark;

import benchmark.support.ProductGroup;
import com.google.common.collect.Sets;
import org.openjdk.jmh.annotations.*;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/*
Benchmark                         Mode  Cnt    Score    Error   Units
SetBenchmark.immSetOfEnum        thrpt    9  213,038 ± 29,901  ops/us
SetBenchmark.enumSet             thrpt    9  210,657 ± 47,328  ops/us
SetBenchmark.setOfString         thrpt    9   84,086 ±  9,753  ops/us
SetBenchmark.setOfEnum           thrpt    9  111,632 ± 27,996  ops/us
SetBenchmark.unmodifSetOfEnum    thrpt    9  209,792 ± 30,277  ops/us
 */
@Warmup(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(3)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Thread)
public class SetBenchmark {
    private final Set<String> setOfString = Set.of("LIGHT", "SHOES", "TIRES", "PHOTO", "PERFUM", "BICYCLE", "WHEELCHAIRS", "MILK", "OTP", "WATER", "TOBACCO", "BEER", "PHARMA");
    private final Set<ProductGroup> enumSet = EnumSet.allOf(ProductGroup.class);
    private final Set<ProductGroup> setOfEnum = Set.copyOf(enumSet);
    private final Set<ProductGroup> unmodifSetOfEnum = Collections.unmodifiableSet(enumSet);
    private final Set<ProductGroup> immEnumSet = Sets.immutableEnumSet(enumSet);

    @Benchmark
    public boolean setOfString() {
        return setOfString.contains("TOBACCO");
    }

    @Benchmark
    public boolean enumSet() {
        return enumSet.contains(ProductGroup.TOBACCO);
    }

    @Benchmark
    public boolean setOfEnum() {
        return setOfEnum.contains(ProductGroup.TOBACCO);
    }

    @Benchmark
    public boolean immSetOfEnum() {
        return immEnumSet.contains(ProductGroup.TOBACCO);
    }

    @Benchmark
    public boolean unmodifSetOfEnum() {
        return unmodifSetOfEnum.contains(ProductGroup.TOBACCO);
    }

}
