package benchmark;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/*
Benchmark                        Mode  Cnt  Score   Error  Units
PatternBenchmark.patternMatcher  avgt    9  0,075 ± 0,008  us/op
PatternBenchmark.stringMatcher   avgt    9  0,217 ± 0,023  us/op
 */
@Warmup(iterations =3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations =3, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(3)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Thread)
public class PatternBenchmark {
    private final Pattern pattern = Pattern.compile("a.*");

    @Benchmark
    public boolean stringMatcher() {
        return "qwyterqwyter".matches("a.*");
    }

    @Benchmark
    public boolean patternMatcher() {
        return pattern.matcher("qwyterqwyter").matches();
    }
}
