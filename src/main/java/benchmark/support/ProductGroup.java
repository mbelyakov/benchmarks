package benchmark.support;

public enum ProductGroup {
    LIGHT, SHOES, TIRES, PHOTO, PERFUM, BICYCLE, WHEELCHAIRS, MILK, OTP, WATER, TOBACCO, BEER, PHARMA
}
