package benchmark.support;

public class FastException extends Exception {
    public FastException() {
        super("message", null, true, false);
    }
}