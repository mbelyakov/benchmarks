package benchmark.support;

public class NoStacktraceException extends Exception {
    @Override
    public synchronized Throwable fillInStackTrace() {
        return null;
    }
}
