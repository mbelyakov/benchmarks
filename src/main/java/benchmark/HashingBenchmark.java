package benchmark;

import com.google.common.hash.Hashing;
import org.openjdk.jmh.annotations.*;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

/*
Benchmark                       (algorithm)   Mode  Cnt  Score   Error   Units
HashingBenchmark.messageDigest       murmur  thrpt    9  8,283 ± 0,823  ops/us
HashingBenchmark.messageDigest          MD5  thrpt    9  3,758 ± 0,641  ops/us
HashingBenchmark.messageDigest        SHA-1  thrpt    9  2,303 ± 0,357  ops/us
HashingBenchmark.messageDigest      SHA-256  thrpt    9  2,383 ± 0,364  ops/us
HashingBenchmark.messageDigest      SHA-384  thrpt    9  2,152 ± 0,203  ops/us
HashingBenchmark.messageDigest      SHA-512  thrpt    9  2,164 ± 0,329  ops/us
 */
@Warmup(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(3)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Thread)
public class HashingBenchmark {
    @Param({"murmur", "MD5", "SHA-1", "SHA-256", "SHA-384", "SHA-512"})
    private String algorithm;
    private Hashable hasher;

    private final byte[] password = "4v3rys3kur3p455w0rd".getBytes(StandardCharsets.UTF_8);

    @Setup(Level.Trial)
    public void setUp() throws NoSuchAlgorithmException {
        if (algorithm.equals("murmur")) {
            hasher = new Murmur();
        } else {
            hasher = new Digest(algorithm);
        }
    }

    @Benchmark
    public byte[] messageDigest() {
        return hasher.hash(password);
    }

    public interface Hashable {
        byte[] hash(byte[] password);
    }

    public static class Digest implements Hashable {
        private final MessageDigest digest;

        public Digest(String algorithm) throws NoSuchAlgorithmException {
            this.digest = MessageDigest.getInstance(algorithm);
        }

        @Override
        public byte[] hash(byte[] password) {
            return digest.digest(password);
        }
    }

    public static class Murmur implements Hashable {
        @Override
        public byte[] hash(byte[] password) {
            return Hashing.murmur3_128().hashBytes(password).asBytes();
        }
    }
}
