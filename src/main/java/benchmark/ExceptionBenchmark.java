package benchmark;

import benchmark.support.FastException;
import benchmark.support.NoStacktraceException;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

/*
Benchmark                                  Mode  Cnt   Score    Error   Units
ExceptionBenchmark.exception              thrpt    9   1,054 ±  0,087  ops/us
ExceptionBenchmark.fastException          thrpt    9  99,332 ± 14,313  ops/us
ExceptionBenchmark.noStacktraceException  thrpt    9  46,937 ±  5,539  ops/us
ExceptionBenchmark.withoutStackTrace      thrpt    9  41,393 ±  7,157  ops/us
 */
@Warmup(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(3)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
public class ExceptionBenchmark {
    @Benchmark
    public Exception exception() {
        return new Exception("");
    }

    @Benchmark
    public Exception withoutStackTrace() {
        return new Exception("") {
            @Override
            public synchronized Throwable fillInStackTrace() {
                return null;
            }
        };
    }

    @Benchmark
    public Exception noStacktraceException() {
        return new NoStacktraceException();
    }

    @Benchmark
    public Exception fastException() {
        return new FastException();
    }
}
