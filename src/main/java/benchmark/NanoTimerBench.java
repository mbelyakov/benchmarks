package benchmark;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

/*
on Battery
Benchmark                   Mode  Cnt    Score    Error  Units
NanoTimerBench.granularity  avgt   25  104,641 ±  1,514  ns/op
NanoTimerBench.latency      avgt   25   45,605 ± 10,105  ns/op

on External Power
Benchmark                   Mode  Cnt    Score   Error  Units
NanoTimerBench.granularity  avgt   25  102,984 ± 0,847  ns/op
NanoTimerBench.latency      avgt   25   21,786 ± 0,138  ns/op

on External Power, 8 threads
Benchmark                   Mode  Cnt    Score   Error  Units
NanoTimerBench.granularity  avgt   25  108,261 ± 5,434  ns/op
NanoTimerBench.latency      avgt   25   37,321 ± 1,271  ns/op
 */

@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@Warmup(iterations = 3, time = 1)
@Measurement(iterations = 3, time = 1)
public class NanoTimerBench {
    private long last;

    @Benchmark
    public long latency() {
        return System.nanoTime();
    }

    @Benchmark
    public long granularity() {
        long lst = last;
        long cur;
        do {
            cur = System.nanoTime();
        } while (cur == lst);
        last = cur;
        return cur;
    }
}